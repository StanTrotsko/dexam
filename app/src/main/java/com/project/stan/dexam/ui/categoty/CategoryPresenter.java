package com.project.stan.dexam.ui.categoty;


import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.project.stan.dexam.models.Category;
import com.project.stan.dexam.service.HttpService;
import com.project.stan.dexam.ui.DataSource;
import com.project.stan.dexam.ui.OnItemClickListener;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class CategoryPresenter implements HttpService.Callback, DataSource<Category>, OnItemClickListener {
    private final CategoryViewable viewable;

    private List<Category> categories;

    public CategoryPresenter(CategoryViewable viewable) {
        this.viewable = viewable;
        categories = new ArrayList<>();
    }

    public void onCreate() {
        HttpService service = new HttpService(this);
        service.execute("http://api.boxycode.com/category");
    }

    @Override
    public void success(String result) {
        Gson gson = new Gson();

        Type listType = new TypeToken<List<Category>>() {}.getType();

        List<Category> resultList = gson.fromJson(result, listType);
        categories.clear();
        for (Category category: resultList) {
            categories.add(category);
        }

        if (categories == null) {
            viewable.setText("Zhopa!");
        } else {
            viewable.setText(null);
            viewable.refresh();
        }
    }

    @Override
    public void failure(Exception e) {

    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public Category getItem(int index) {
        return categories.get(index);
    }

    @Override
    public void onClick(View v, int position) {
        viewable.launchMain(categories.get(position).getId());
    }
}
