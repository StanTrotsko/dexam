package com.project.stan.dexam.ui.categoty;


public interface CategoryViewable {
    void setText(String value);

    void refresh();

    void launchMain(int categoryId);
}
