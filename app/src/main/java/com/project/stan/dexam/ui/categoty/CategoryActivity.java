package com.project.stan.dexam.ui.categoty;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.project.stan.dexam.R;
import com.project.stan.dexam.ui.Const;
import com.project.stan.dexam.ui.main.MainActivity;

public class CategoryActivity extends AppCompatActivity implements CategoryViewable {

    private CategoryPresenter presenter;
    private TextView textView;
    private RecyclerView listView;
    private CategoryListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        presenter = new CategoryPresenter(this);
        initViews();
        presenter.onCreate();
    }

    private void initViews() {
        textView = (TextView) findViewById(R.id.activity_category_text);
        listView = (RecyclerView) findViewById(R.id.activity_category_list);
        adapter = new CategoryListAdapter(listView, presenter, presenter);
        listView.setAdapter(adapter);
        listView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void setText(String value) {
        //textView.setText(value);
    }

    @Override
    public void refresh() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void launchMain(int categoryId) {
        final Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(Const.KEY_CATEGORY_ID, categoryId);
        startActivity(intent);
    }
}
