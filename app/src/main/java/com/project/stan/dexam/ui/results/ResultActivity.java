package com.project.stan.dexam.ui.results;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;
import com.project.stan.dexam.R;

public class ResultActivity extends AppCompatActivity implements ResultViewable{

    private ResultPresenter presenter;
    private TextView txtResult;
    private RecyclerView resultsRecyclerView;
    private ResultsListAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        Bundle container = getIntent().getExtras();
        presenter = new ResultPresenter(this);

        presenter.onCreate(container);
        initViews();
    }

    private void initViews() {
        txtResult = (TextView)findViewById(R.id.result_text);
        resultsRecyclerView = (RecyclerView)findViewById(R.id.activity_result_results);
        resultsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ResultsListAdapter(this, presenter);
        resultsRecyclerView.setAdapter(adapter);

    }
}
