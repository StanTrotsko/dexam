package com.project.stan.dexam.models;

public class Category {

    private int id;
    private String age;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return age;
    }

    public void setName(String name) {
        this.age = name;
    }
}
