package com.project.stan.dexam.ui;


public interface Const {
    String KEY_CATEGORY_ID = "catId";
    String KEY_ANSWERS = "answers";
    String KEY_QUIZES = "quizes";
}
