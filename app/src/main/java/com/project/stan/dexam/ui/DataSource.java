package com.project.stan.dexam.ui;

public interface DataSource<T> {

    int getCount();
    T getItem(int index);
}
