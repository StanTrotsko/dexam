package com.project.stan.dexam.ui.main;


import android.os.Bundle;
import android.view.View;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.project.stan.dexam.models.Quiz;
import com.project.stan.dexam.service.HttpService;
import com.project.stan.dexam.ui.Const;
import com.project.stan.dexam.ui.DataSource;
import com.project.stan.dexam.ui.OnItemClickListener;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


public class MainPresenter implements HttpService.Callback, DataSource<String>, OnItemClickListener{


    MainViewable viewable;
    private int categoryId;
    private int quizIndex = 0;
    private final ArrayList<Quiz> quizes;
    private String correct;


    public MainPresenter(MainViewable viewable){
        this.viewable = viewable;
        quizes = new ArrayList<>();
    }

    public void onCreate(Bundle state, Bundle extras) {
        final Bundle bundle = (state == null) ? extras : state;
        categoryId = bundle.getInt(Const.KEY_CATEGORY_ID);
        if (bundle.getParcelableArrayList(Const.KEY_QUIZES) != null) {
            quizes.clear();
            List<Quiz> recovered = bundle.getParcelableArrayList(Const.KEY_QUIZES);
            for (Quiz quiz: recovered) {
                quizes.add(quiz);
            }
        }

        if (quizes.size() == 0) {
            executeQuizQuery();
        }
    }

    private void executeQuizQuery() {
        String url = "http://api.boxycode.com/category/" + categoryId;
        HttpService service = new HttpService(this);
        service.execute(url);
    }

    public void onSaveState(Bundle state) {
        state.putInt(Const.KEY_CATEGORY_ID, categoryId);
        state.putParcelableArrayList(Const.KEY_QUIZES, quizes);
    }

    @Override
    public void success(String result) {
        Gson gson = new Gson();

        Type listType = new TypeToken<List<Quiz>>() {}.getType();

        List<Quiz> resultList = gson.fromJson(result, listType);
        quizes.clear();
        for (Quiz quiz: resultList) {
            quizes.add(quiz);
        }

        correct = quizes.get(quizIndex).getCorrect();
        showQuiz(quizes.get(quizIndex));
    }

    private void showQuiz(Quiz quiz) {
        viewable.showQuestion(quiz.getQuestion());
        viewable.showImage(quiz.getImage());
        viewable.refreshAnswers();
    }

    @Override
    public void failure(Exception e) {

    }

    @Override
    public int getCount() {
        if (quizes.size() > quizIndex) {
            final Quiz quiz = quizes.get(quizIndex);
            return quiz.getAnswers().size();
        }
        return 0;
    }

    @Override
    public String getItem(int index) {
        final Quiz quiz = quizes.get(quizIndex);
        return quiz.getAnswers().get(index);
    }

    @Override
    public void onClick(View v, int position) {

        String user_answer;
        user_answer = quizes.get(quizIndex).getAnswers().get(position);
        quizes.get(quizIndex).setUserAnswer(user_answer);


        if (validateAnswer(user_answer)) {
            viewable.displayCorrect();
        }
        else{
            viewable.displayWrong();
        }
    }

    public void updateQuestion(){
            this.quizIndex++;
            showQuiz(quizes.get(quizIndex));
            correct = quizes.get(quizIndex).getCorrect();
    }

    private Boolean validateAnswer(String userAnswer){
        if(correct.equals(userAnswer)){
            return true;
        }
        else {
            return false;
        }
    }

    public void onQuestionResultClick() {
        viewable.hideExplanation();
        if (quizIndex+1 < quizes.size()) {

            updateQuestion();
        } else {
            viewable.startResultPage(categoryId, quizes);
        }
    }
}
