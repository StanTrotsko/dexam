package com.project.stan.dexam.ui.results;

import android.os.Bundle;

import com.project.stan.dexam.models.Quiz;
import com.project.stan.dexam.ui.Const;
import com.project.stan.dexam.ui.DataSource;

import java.util.ArrayList;

public class ResultPresenter implements DataSource<Quiz> {

    private ResultViewable viewable;
    private ArrayList<Quiz> quizes;


    public ResultPresenter(ResultViewable viewable) {
        this.viewable = viewable;
    }

    public void onCreate(Bundle container) {
        quizes = container.getParcelableArrayList(Const.KEY_QUIZES);
    }


    @Override
    public int getCount() {
        return quizes.size();
    }

    @Override
    public Quiz getItem(int index) {
        return quizes.get(index);
    }

}
