package com.project.stan.dexam.ui.results;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.stan.dexam.R;
import com.project.stan.dexam.models.Quiz;
import com.project.stan.dexam.ui.DataSource;


public class ResultsListAdapter extends RecyclerView.Adapter {

    private  static DataSource<Quiz> quizes;
    private static Context context;

    public ResultsListAdapter(Context context, DataSource<Quiz> quizes){
        this.context = context;
        this.quizes =  quizes;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.results_list_item, parent, false);
        return new ResultViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        String answer = quizes.getItem(position).getUserAnswer();
        String question = quizes.getItem(position).getQuestion();
        ((ResultViewHolder)holder).updateView(position, answer, question);

    }

    @Override
    public int getItemCount() {
        return quizes.getCount();
    }

    private class ResultViewHolder extends RecyclerView.ViewHolder{
        private final ImageView icon;
        private final TextView text_q;
        private final TextView text_a;

        public ResultViewHolder(View itemView) {
            super(itemView);
            icon = (ImageView)itemView.findViewById(R.id.result_icon);
            text_q = (TextView)itemView.findViewById(R.id.result_item_text_q);
            text_a = (TextView)itemView.findViewById(R.id.result_item_text_a);
        }

        public void updateView(int position, String answer, String question) {
            text_q.setText("Q: " + question);
            text_a.setText("A: " + answer);
            icon.setImageBitmap(setIcon(position));
        }

        private Bitmap setIcon(int position){
            String correctAnswer = ResultsListAdapter.quizes.getItem(position).getCorrect();
            String userAnswer = ResultsListAdapter.quizes.getItem(position).getUserAnswer();

            if(userAnswer.equals(correctAnswer)){
              return BitmapFactory.decodeResource(context.getResources(), R.drawable.correct);
            }
            else{
                return BitmapFactory.decodeResource(context.getResources(), R.drawable.wrong);
            }
        }

    }




}
