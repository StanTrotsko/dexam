package com.project.stan.dexam.ui.start;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import com.project.stan.dexam.R;
import com.project.stan.dexam.ui.categoty.CategoryActivity;


public class StartActivity extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
    }

    public void startTest(View v){
        Intent intent = new Intent(this, CategoryActivity.class);
        startActivity(intent);
    }
}
