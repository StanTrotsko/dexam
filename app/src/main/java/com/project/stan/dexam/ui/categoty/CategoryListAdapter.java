package com.project.stan.dexam.ui.categoty;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.project.stan.dexam.R;
import com.project.stan.dexam.models.Category;
import com.project.stan.dexam.ui.DataSource;
import com.project.stan.dexam.ui.OnItemClickListener;

public class CategoryListAdapter extends RecyclerView.Adapter {

    private final DataSource<Category> dataSource;
    private final OnItemClickListener clickListener;
    private final RecyclerView recyclerView;
    private View.OnClickListener listener;


    public CategoryListAdapter(RecyclerView recyclerView, DataSource<Category> dataSource, final OnItemClickListener clickListener) {
        this.dataSource = dataSource;
        this.clickListener = clickListener;
        this.recyclerView = recyclerView;
        listener = new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                final int position = CategoryListAdapter.this.recyclerView.getChildAdapterPosition(view);
                clickListener.onClick(view, position);
            }
        };
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.part_category_list_item, parent, false);
        view.setOnClickListener(listener);
        return new CategoryItemHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final Category category = dataSource.getItem(position);
        ((CategoryItemHolder) holder).updateView(category);
    }

    @Override
    public int getItemCount() {
        return dataSource.getCount();
    }


    private static class CategoryItemHolder extends RecyclerView.ViewHolder {
        private final TextView textView;

        public CategoryItemHolder(View view) {
            super(view);
            textView = (TextView) view.findViewById(R.id.part_category_list_item_text);
        }

        public void updateView(Category category) {
            textView.setText(category.getName());
        }
    }
}
