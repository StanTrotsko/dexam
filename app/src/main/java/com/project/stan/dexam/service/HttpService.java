package com.project.stan.dexam.service;

import android.os.AsyncTask;

import com.project.stan.dexam.ui.categoty.CategoryPresenter;

import java.io.IOException;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class HttpService extends AsyncTask<String, Void, String>  {

    public interface Callback {
        void success(String result);
        void failure(Exception e);
    }

    private final Callback callback;

    public HttpService(Callback callback) {
        this.callback = callback;
    }

    @Override
    protected String doInBackground(String... value) {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(value[0])
                .build();

        Response response = null;
        try{
            response = client.newCall(request).execute();
            return response.body().string();
        } catch (IOException e){
            callback.failure(e);
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        callback.success(result);
    }
}
