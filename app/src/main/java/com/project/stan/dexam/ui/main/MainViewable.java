package com.project.stan.dexam.ui.main;

import com.project.stan.dexam.models.Quiz;
import java.util.ArrayList;

public interface MainViewable {

    void showQuestion(String question);

    void showImage(String url);

    void refreshAnswers();

    void displayCorrect();

    void displayWrong();

    void startResultPage(int categoryId, ArrayList<Quiz> quizes);

    void hideExplanation();
}
