package com.project.stan.dexam.ui.main;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.project.stan.dexam.R;
import com.project.stan.dexam.ui.DataSource;
import com.project.stan.dexam.ui.OnItemClickListener;

public class AnswersListAdapter extends RecyclerView.Adapter {
    private final DataSource<String> dataSource;
    private View.OnClickListener listener;

    public AnswersListAdapter(final RecyclerView recyclerView, DataSource<String> dataSource, final OnItemClickListener onItemClickListener) {
        this.dataSource = dataSource;
        listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position =  recyclerView.getChildAdapterPosition(v);
                onItemClickListener.onClick(v, position);
            }
        };
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.part_answers_list_item, parent, false);
        view.setOnClickListener(listener);
        return new AnswerItemHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        String answer = dataSource.getItem(position);
        ((AnswerItemHolder)holder).updateView(position, answer);
    }

    @Override
    public int getItemCount() {
        return dataSource.getCount();
    }


    private static class AnswerItemHolder extends RecyclerView.ViewHolder {
        private final TextView indexView;
        private final TextView answerView;

        public AnswerItemHolder(View view) {
            super(view);
            indexView = (TextView) view.findViewById(R.id.part_answers_list_item_index);
            answerView = (TextView) view.findViewById(R.id.part_answers_list_item_text);
        }

        public void updateView(int position, String answer) {
            indexView.setText(Integer.toString(position + 1) + ".");
            answerView.setText(answer);
        }
    }
}
