package com.project.stan.dexam.ui.main;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.project.stan.dexam.R;
import com.project.stan.dexam.models.Quiz;
import com.project.stan.dexam.ui.Const;
import com.project.stan.dexam.ui.results.ResultActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements MainViewable {

    private MainPresenter presenter;
    private TextView questionView;
    private ImageView imageView;
    private RecyclerView answersView;
    private AnswersListAdapter adapter;
    //display correct/wrong window
    private LinearLayout answerExplanation;
    private ImageView answerIcon;
    private TextView answerHeaderText;
    private TextView answerMainText;
    private View tint;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        presenter = new MainPresenter(this);
        initViews();
        presenter.onCreate(savedInstanceState, getIntent().getExtras());
    }

    private void initViews() {
        questionView = (TextView)findViewById(R.id.activity_main_question);
        imageView = (ImageView) findViewById(R.id.activity_main_image);
        answersView = (RecyclerView) findViewById(R.id.activity_main_answers);
        answersView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new AnswersListAdapter(answersView, presenter, presenter);
        answersView.setAdapter(adapter);
        //display correct/wrong explanation window
        answerExplanation = (LinearLayout)findViewById(R.id.layout_answer_explanation);
        answerIcon = (ImageView)findViewById(R.id.layout_answer_explanation_header_image);
        answerHeaderText = (TextView)findViewById(R.id.layout_answer_explanation_header_text);
        answerMainText = (TextView)findViewById(R.id.layout_answer_explanation_main_text);
        tint = (View)findViewById(R.id.activity_main_tint);
        answerMainText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onQuestionResultClick();
            }
        });

        tint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        presenter.onSaveState(outState);
    }

    @Override
    public void showQuestion(String question) {
        questionView.setText(question);
    }

    @Override
    public void showImage(String url) {
        Picasso.with(this).load(url).into(imageView);
    }

    @Override
    public void refreshAnswers() {
        adapter.notifyDataSetChanged();
    }

    private void displayExplanation(Bitmap image, String header_text, String main_text){

        int parentW = ((FrameLayout)answerExplanation.getParent()).getWidth();
        int parentH = ((FrameLayout)answerExplanation.getParent()).getHeight();
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams((int)(parentW*.7), (int)(parentH*.5));
        params.gravity = Gravity.CENTER;
        answerExplanation.setLayoutParams(params);

        answerIcon.setImageBitmap(image);
        answerHeaderText.setText(header_text);
        answerMainText.setText(main_text);
        answerExplanation.setVisibility(View.VISIBLE);
        tint.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideExplanation(){
        answerExplanation.setVisibility(View.INVISIBLE);
        tint.setVisibility(View.INVISIBLE);
    }


    @Override
    public void displayCorrect() {

        Bitmap image = BitmapFactory.decodeResource(this.getResources(), R.drawable.correct);
        displayExplanation(image, "Correct answer!", "Explanation...");

    }

    @Override
    public void displayWrong() {
        Bitmap image = BitmapFactory.decodeResource(this.getResources(), R.drawable.wrong);
        displayExplanation(image, "Wrong answer!", "Explanation...");
    }

    @Override
    public void startResultPage(int categoryId, ArrayList<Quiz> quizes) {
        Intent intent = new Intent(this, ResultActivity.class);
        intent.putExtra(Const.KEY_CATEGORY_ID, categoryId);
        intent.putExtra(Const.KEY_QUIZES, quizes);
        startActivity(intent);
    }
}
