package com.project.stan.dexam.ui;

import android.view.View;

public interface OnItemClickListener {
    void onClick(View v, int position);
}
